<?php

namespace App\Http\Controllers;

use App\Partner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    public function index()
    {
        $currentUser = Auth::user()->getId();

        $projects = Partner::where('user_id', $currentUser)
            ->orderBy('created_at', 'desc')
            ->get();

        return $projects->toJson();
    }

    public function store(Request $request)
    {
        if (Auth::check()) {
            $userId = Auth::user()->getId();
        }

        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        $partner = new Partner();
        $partner->user_id = $userId;
        $partner->name = $validatedData['name'];
        $partner->save();
        return response()->json($partner);
    }
}
