<?php

namespace App\Http\Controllers;

use App\MeetPartner;
use App\Partner;
use Illuminate\Http\Request;
use App\Meet;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;


class MeetController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $currentUser = Auth::user()->getId();
        $meets = Meet::where('user_id',$currentUser)
            ->with('meetsPartners')
//            ->with('partners')
            ->orderBy('time_meet', 'desc')
            ->get();

        return $meets->toJson();
    }

    public function store(Request $request)
    {

       if (Auth::check())
        {
            $userId = Auth::user()->getId();
        }

       //        Log::info($request['selectedOption']);

               $validatedData = $request->validate([
                   'time_meet' => 'required',
                   'selectedOption'=> 'required',
               ]);

               $meet = new Meet();
               $meet->user_id = Auth::user()->getId();
               $meet->time_meet = Carbon::parse($validatedData['time_meet']);
               $meet->place = "";
               $meet->save();
//               Log::info($meet->id);
$meetId = $meet->id;

        foreach ($validatedData['selectedOption'] as $item) {
//            Log::info($item['label']);
            $partner = Partner::firstOrNew(
                ['name' => $item['label']], ['user_id' => $userId]
            );
            Log::info($partner);
            $partner->save();
                $meetPartner = new MeetPartner();
                $meetPartner->meet_id = $meetId;
                $meetPartner->partner_id = $partner->id;
                $meetPartner->save();
        }
//
//        $meet = Meet::create([
//            'time_meet' => $validatedData['time_meet'],
//            'place' => $validatedData['place'],
//            'user_id'=> Auth::user()->getId()
//        ]);
        $meet = Meet::where('id',$meetId)
            ->with('meetsPartners')
            ->get();


        return response()->json($meet);
    }

    public function show($id)
    {
        $meet = Meet::with(['meets_partners' => function ($query) {
            $query->where('meet_id', false);
        }])->find($id);

        return $meet->toJson();
    }

    public function update($id){
        //
    }

    public function delete($id){
        //
    }
}
