<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetPartner extends Model
{
    protected $fillable = ['meet_id', 'partner_id'];

    protected $table = 'meets_partners';

    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }
}
