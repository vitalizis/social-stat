<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meet extends Model
{
    protected $fillable = ['time_meet', 'place','user_id'];

    public function meetsPartners()
    {
        return $this->hasMany('App\MeetPartner')->with('partner');
    }

    public function partners()
    {
        return $this->hasManyThrough('App\Partner', 'App\MeetPartner');
    }
}
