import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios'
import Select from 'react-select';
import CreatableSelect from 'react-select/lib/Creatable';

import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";


class InputItem extends Component {
  
    state = {
        selectedOption: null,
        options: [],
      }

      handleChange = (selectedOption) => {
        this.setState({ selectedOption });
        // console.log(`Option selected:`, selectedOption);
        this.addMeet = this.addMeet.bind(this);
      }

      constructor(props) {
        super(props);
        this.state = {
          startDate: new Date(),
        };
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.addMeet = this.addMeet.bind(this);
      }

      componentDidMount () {
        axios.get('/api/partners/').then(response => {
          let partners = response.data.map( category => ({ value: category.id, label: category.name }));
          this.setState({
            options: partners
          })
          // console.log(this.state.options);
        })

        axios.get('/api/meets/').then(response => {
          console.log(response.data);
          // let partners = response.data.map( category => ({ value: category.id, label: category.name }));
          // this.setState({
          //   options: partners
          // })
          // console.log(this.state.options);
        })
      }


      handleChangeDate(date) {
        this.setState({
          startDate: date
        });
        // console.log(this.state.startDate);
      }

      addMeet(){
        console.log(this.state.selectedOption);
        console.log(this.state.options);

          const meet = {
            time_meet: this.state.startDate,
            selectedOption: this.state.selectedOption,
          }
        // console.log('time_meet: '+ this.state.startDate);
        // console.log('opinion: '+ this.state.selectedOption[0].value);
        // window.axios.defaults.headers.common = {
        //     'X-Requested-With': 'XMLHttpRequest',
        // };
        axios.post('/api/meets/', meet)
        .then(response => {
          // redirect to the homepage
        //   history.push('/')
        })
        .catch(error => {
          this.setState({
            errors: error.response.data.errors
          })
        })
      }

        render() {
    const { selectedOption } = this.state;

    return (
        // <form onSubmit={this.addMeet}>
        <div className="input-item-component">
        <h1>Add meet</h1>
        {/* <input type="text" className="form-control add-todo" placeholder="Add todo"/> */}
        <div className="row">
        <div className="col-md-6">
        <CreatableSelect
isMulti
onChange={this.handleChange}
options={this.state.options}
/></div>
        <div className="col-md-6">
          <DatePicker className="form-control"
selected={this.state.startDate}
onChange={this.handleChangeDate}
/> 
        </div>                    
        </div>   
        <div className="row">
           <div className="col-md-4">
           </div>
           <div className="col-md-4">
              <button id="checkAll" onClick={this.addMeet} className="btn btn-success" >Add</button>
           </div>
           <div className="col-md-4">
           </div>
        </div>
        <hr/>
      </div>
      /* </form> */
    );
  }
}

export default InputItem