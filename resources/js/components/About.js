import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Header from './Header'

class About extends Component {
  render () {
    return (
<div className="about">
   <div>
      <div className="container">
         <div className="row">
            <div className="col-md-12">
               <div className="todolist not-done">
                <Header />
                <br/>
                <div className="main-list">
                   <div className="input-item-component">
        <h1>About</h1>
        </div>
        <p>Your salary is equal to the average salary of 5 people with whom you communicate the most. 
            This suggests that it is important to control with whom you communicate the most.
             But it is not only useful, but also interesting.</p>
            <p>This application will help you keep track of who you communicate with the most,
                 providing a convenient interface for entering information about each meeting. 
                The information is very simple in structure: with whom you met and when you met.</p>
                <p>
                This information is available only for your profile that you are creating soon and there is no one for whom.
                </p>
        </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div>
   </div>
</div>
    )
  }
}

export default About;