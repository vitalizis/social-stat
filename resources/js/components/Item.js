import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios'
import Select from 'react-select';
import CreatableSelect from 'react-select/lib/Creatable';

import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";


class Item extends Component {
    constructor(props){
        super(props);
    }
render(){
    let {meets_partner} = this.props;
    let {time_meet} = this.props;
    let partners = meets_partner.map( category => ({ value: category.partner.id, label: category.partner.name }));
    return(
        <li >
                        <div className="form-group">
        <label htmlFor="formGroupExampleInput">Дата встречи: </label><br/>
        <DatePicker className="form-control"
selected={time_meet}
/> 
</div>
                <div className="form-group">
        <label htmlFor="formGroupExampleInput">Список партнеров: </label>
        <CreatableSelect
isMulti
defaultValue={partners}
/>
<hr/>
        </div>
     </li>
    );
}
}

export default Item;