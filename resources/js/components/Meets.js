import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios'
import Select from 'react-select';
import CreatableSelect from 'react-select/lib/Creatable';

import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

import Item from './Item';

class Meets extends Component {

   constructor (props) {
      super(props)
  
      this.state = {
        meets: [],
        errors: [],
        selectedOption: null,
        options: [],
        startDate: new Date(),
      }
       this.handleChangeDate = this.handleChangeDate.bind(this);
       this.addMeet = this.addMeet.bind(this);
   }

   handleChange = (selectedOption) => {
      this.setState({ selectedOption });
      console.log(`Option selected:`, selectedOption);
      this.addMeet = this.addMeet.bind(this);
    }

   componentDidMount () {

      axios.get('/api/partners/').then(response => {
         let partners = response.data.map( category => ({ value: category.id, label: category.name }));
         this.setState({
           options: partners
         })

       })
  
      axios.get(`/api/meets/`).then(response => {
        this.setState({
          meets: response.data,
        })
      })
    }
      handleChangeDate(date) {
        this.setState({
          startDate: date
        });

      }

      addMeet(){
          const meet = {
            time_meet: this.state.startDate,
            selectedOption: this.state.selectedOption,
          }
        axios.post('/api/meets/', meet)
        .then(response => {

        
        this.setState(prevState => ({
          meets: prevState.meets.concat(response.data)
        }))
        })
        .catch(error => {
          this.setState({
            errors: error.response.data.errors
          })
        })
        
        this.setState({
         selectedOption: null,
       }) 
      }
    render(){

      const { meets } = this.state

        return(
           <div className="main-list">
                   <div className="input-item-component">
        <h1>Add meet</h1>
        {/* <input type="text" className="form-control add-todo" placeholder="Add todo"/> */}
        <div className="row">
        <div className="col-md-6">
        <CreatableSelect
isMulti
onChange={this.handleChange}
options={this.state.options}
value={this.state.selectedOption}
/></div>
        <div className="col-md-6">
          <DatePicker className="form-control"
selected={this.state.startDate}
onChange={this.handleChangeDate}
/> 
        </div>                    
        </div>   
        <div className="row">
           <div className="col-md-4">
           </div>
           <div className="col-md-4">
              <button id="checkAll" onClick={this.addMeet} className="btn btn-success" >Add</button>
           </div>
           <div className="col-md-4">
           </div>
        </div>
        <hr/>
      </div>
<div className="list-items-component">
<ul id="sortable" className="list-unstyled">
 { meets.map(meet => (
<Item meets_partner={meet.meets_partners} time_meet={meet.time_meet} key={meet.id}/>
   ))}
</ul>
</div>
</div>
        );
    }
}

export default Meets




