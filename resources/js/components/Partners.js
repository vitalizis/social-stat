import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import axios from 'axios';

import Header from './Header'

class Partners extends Component {
    constructor (props) {
        super(props)
    
        this.state = {
          partners: [],
          name: '',
          errors: []
        }

        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleAddNewPartner = this.handleAddNewPartner.bind(this)
      }
    componentDidMount () {
        const projectId = this.props.match.params.id
    
        axios.get('/api/partners/').then(response => {
          this.setState({
            partners: response.data
          })
        })
      }
      handleFieldChange (event) {
        this.setState({
          name: event.target.value
        })
      }

      handleAddNewPartner (event) {
        event.preventDefault()
    
        const partner = {
          name: this.state.name,
        }
    
        axios
          .post('/api/partners', partner)
          .then(response => {
            // clear form input
            this.setState({
              name: ''
            })
            this.setState(prevState => ({
              partners: prevState.partners.concat(response.data)
            }))
          })
          .catch(error => {
            this.setState({
              errors: error.response.data.errors
            })
          })
      }

  render () {
    const { partners } = this.state
    return (
<div>
   <div>
      <div className="container">
         <div className="row">
            <div className="col-md-12">
               <div className="todolist not-done">
                <Header />
                <br/>
                <div className="add-block">
  <div className="form-group">
    <label>Add partner</label>
                    <input
                      className="form-control"
                      type='text'
                      name='name'
                    //   className={`form-control ${this.hasErrorFor('title') ? 'is-invalid' : ''}`}
                      placeholder='Partner name'
                      value={this.state.name}
                      onChange={this.handleFieldChange}
                    />
  </div>
                                        <div className='input-group-append' onClick={this.handleAddNewPartner}>
                      <button className='btn btn-primary'>Add</button>
                    </div>
                    </div>
                <ul className='list-group mt-3'>
                  {partners.map(partner => (
                    <li
                      className='list-group-item d-flex justify-content-between align-items-center'
                      key={partner.id}
                    >
                      {partner.name}
                    </li>
                  ))}

                </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div>
   </div>
</div>
    )
  }
}

export default Partners;