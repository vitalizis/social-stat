import React from 'react'
import { Link } from 'react-router-dom'

const Header = () => (
  <nav className='navbar navbar-expand-md navbar-light navbar-laravel'>
  <div className='container'>
    <div className="col-md-4"><Link className='navbar-brand' to='/about'>About</Link></div>
    <div className="col-md-4"><Link className='navbar-brand' to='/'>Add Meet</Link></div>
    <div className="col-md-4"><Link className='navbar-brand' to='/partners'>Partners</Link></div>
  </div>
</nav>
)

export default Header