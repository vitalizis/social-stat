<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeersPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meets_partners', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('meet_id');
            $table->foreign('meet_id')->references('id')->on('meets');

            $table->unsignedInteger('partner_id');
            $table->foreign('partner_id')->references('id')->on('partners');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meets_partners');
    }
}
