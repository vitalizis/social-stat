<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('meets', 'MeetController@index');
Route::get('meets/{id}', 'MeetController@show');
Route::post('meets', 'MeetController@store');
Route::put('meets/{id}', 'MeetController@update');
Route::delete('meets/{id}', 'MeetController@delete');


Route::get('partners', 'PartnerController@index');
Route::get('partners/{id}', 'PartnerController@show');
Route::post('partners', 'PartnerController@store');
Route::put('partners/{id}', 'PartnerController@update');
Route::delete('partners/{id}', 'PartnerController@delete');

Route::get('meets-partners', 'MeetPartnerController@index');
Route::get('meets-partners/{id}', 'MeetPartnerController@show');
Route::post('meets-partners', 'MeetPartnerController@store');
Route::put('meets-partners/{id}', 'MeetPartnerController@update');
Route::delete('meets-partners/{id}', 'MeetPartnerController@delete');